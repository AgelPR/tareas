<?php require_once ("database/config.php") ?>
<?php require_once ("includes/header.php") ?>
	
<!--CONTAINER-->
	<div class="container p-4">
		<div class="row">
			<!--CONTENEDOR DE LA TARJETA DE LA TAREA-->
			<div class="col-md-4">
				<!--ALERTA-->
				 <?php if(isset($_SESSION['message'])) { ?>
				 	<div class="alert alert-<?=$_SESSION['message-type'];?> alert-dismissible fade show" role="alert">
				 		<?= $_SESSION['message'] ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
				 <?php session_unset(); } ?>
				<!--FIN ALERTA-->
				<div class="card card-body">
					<form action="save_task.php" method="POST">
						<div class="form-group">
							<input type="text" name="title" class="form-control" placeholder="Task Title" autofocus>
						</div><br>
						<div class="form-group">
							<textarea name="description" rows="2" class="form-control" placeholder="Task Description"></textarea>
						</div><br> 
						<div class="form-group col text-center">
							<button type="submit" class="btn btn-success" name="save">Create Task</button>
						</div>
					</form>
				</div>
			</div>
			<!--FIN DEL CONTENEDOR DE LA TARJETA DE LA TAREA-->

			<!--CONTENEDOR DE LA TABLA DE LAS TAREAS-->
			<div class="col-md-8">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Title</th>
							<th>Description</th>
							<th>Created At</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$sql = "SELECT * FROM tareas";
							$resultado = mysqli_query($conn, $sql);

							while ($filas = mysqli_fetch_array($resultado)) { ?>
								<tr>
									<td><?php echo $filas['title']; ?></td>
									<td><?php echo $filas['description']; ?></td>
									<td><?php echo $filas['create_ad']; ?></td>
									<td>
										<!--Agregar iconos usando Font Awesoma min 45-->
										<a href="edit.php?id=<?= $filas['id'];?>" class="btn btn-primary">
											<i class="fas fa-edit"></i>
										</a>
										<a href="delete.php?id=<?= $filas['id'];?>" class="btn btn-danger">
											<i class="far fa-trash-alt"></i>
										</a>
									</td>
								</tr>
							<?php } ?>
					</tbody>
				</table>
			</div>
			<!--FIN CONTENEDOR DE LA TABLA DE LAS TAREAS-->
		</div>
	</div>
<!--END CONTAINER-->

<?php require_once ("includes/footer.php") ?>