<?php
require_once ("database/config.php");
if (isset($_GET['id'])) {
	$numero = $_GET['id'];
	$sql = "SELECT * FROM tareas WHERE id = '$numero'";
	$resultado = mysqli_query($conn, $sql);

	if (mysqli_num_rows($resultado) == 1 ) {
		$filas = mysqli_fetch_array($resultado);
		$titulo = $filas['title'];
		$descripcion = $filas['description'];
	}
 }

 if (isset($_POST['update'])) {
 	$numero = $_GET['id'];
 	$up_titulo = $_POST['up_title'];
 	$up_descripcion = $_POST['up_description'];

 	$sql = "UPDATE tareas SET title = '$up_titulo', description = '$up_descripcion' WHERE id = '$numero'";
 	mysqli_query($conn, $sql);

 	$_SESSION['message'] = 'Task updated successfully';
	$_SESSION['message-type'] = 'info';

 	header("Location:index.php");
 }		
?>

<?php require_once ("includes/header.php") ?>
<div class="container p-4">
	<div class="row">
		<div class="col-md-4 mx-auto">
			<div class="card card-body">
				<form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
					<div class="form-group">
						<input type="text" name="up_title" class="form-control" placeholder="Actualiza el titulo" value="<?php echo $titulo; ?>">
					</div><br>
					<div class="form-group">
						<textarea row="2" class="form-control" name="up_description" placeholder="Actualizar descripción"><?php echo $descripcion; ?></textarea>
					</div><br>
					<div class="form-group col text-center">
							<button type="submit" class="btn btn-primary" name="update">Update Task</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php require_once ("includes/footer.php") ?>